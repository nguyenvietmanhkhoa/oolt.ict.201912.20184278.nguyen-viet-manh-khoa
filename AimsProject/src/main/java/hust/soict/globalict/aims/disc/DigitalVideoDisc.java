package hust.soict.globalict.aims.disc;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author admin
 */
public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the director
     */
    public String getDirector() {
        return director;
    }

    /**
     * @param director the director to set
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the cost
     */
    public float getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(float cost) {
        this.cost = cost;
    }
    public DigitalVideoDisc(String title)
    {
        this.title = title;
    }
    public DigitalVideoDisc(String category,String title)
    {
        this.category = category;
        this.title = title;
    }
    public DigitalVideoDisc(String director,String category,String title)
    {
        this.director = director;
        this.category = category;
        this.title = title;
    }
    public DigitalVideoDisc(String director,String category,String title,int length,float cost)
    {
        this.director = director;
        this.category = category;
        this.title = title;
        this.length = length;
        this.cost = cost;
    }
    public boolean search(String title)
    {
       String[] array_title  = title.split(" ");
       String temp = this.getTitle().toLowerCase();
       for(String i : array_title)
       {
           i = i.toLowerCase();
           if(!temp.contains(i))
           {
               return false;
           }
       }
       return true;
    }
}
