package hust.soict.globalict.aims.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;
import java.text.SimpleDateFormat;  
import java.util.Date;  

/**
 *
 * @author admin
 */
public class MyDate {
    private int day;
    private int month;
    private int year;

    /**
     * @return the day
     */
    public int getDay() {
        return day;
    }

    /**
     * @param day the day to set
     */
    public void setDay(int day) {
        boolean Check = false;
	if(this.month == 1 || this.month == 3 || this.month == 5 || this.month == 7 || this.month == 8 || this.month == 10 || this.month == 12)
	{
            if(day >=1 && day <=31)
            {
                this.day = day;
                Check = true;
            }
	}
	else if(this.month == 4 || this.month == 6 || this.month == 9 || this.month == 11)
	{
	    if(day >=1 && day <=30)
            {
                this.day = day;
                Check = true;
            }
	}
	else
	{
	    if(Check_year())
	    {
		if(day >= 1 && day <= 29)
                {
                    this.day = day;
                    Check = true;
                }
	    }
	    else
	    {
                if(day >=1 && day <= 28)
                {
                    this.day = day;
                    Check = true;
                }
	    }
	}
        if(!Check)
        {
            System.out.println("\nDay is not valid");
        }
    }
    public void setDay(String day)
    {
        if(day == "first")
        {
            this.day = 1;
        }
        else if(day == "second")
        {
            this.day = 2;
        }
        else if(day == "third")
        {
            this.day = 3;
        }
        else if(day == "fourth")
        {
            this.day = 4;
        }
        else if(day == "fifth")
        {
            this.day = 5;
        }
        else if(day == "sixth")
        {
            this.day = 6;
        }
        else if(day == "seventh")
        {
            this.day = 7;
        }
        else if(day == "eighth")
        {
            this.day = 8;
        }
        else if(day == "ninth")
        {
            this.day = 9;
        }
        else if(day == "tenth")
        {
            this.day = 10;
        }
        else if(day == "eleventh")
        {
            this.day = 11;
        }
        else if(day == "twelfth")
        {
            this.day = 12;
        }
        else if(day == "thirteenth")
        {
            this.day = 13;
        }
        else if(day == "fourteenth")
        {
            this.day = 14;
        }
        else if(day == "fifteenth")
        {
            this.day = 15;
        }
        else if(day == "sixteentth")
        {
            this.day = 16;
        }
        else if(day == "seventeenth")
        {
            this.day = 17;
        }
        else if(day == "eighteenth")
        {
            this.day = 18;
        }
        else if(day == "nineteenth")
        {
            this.day = 19;
        }
        else if(day == "twentieth")
        {
            this.day = 20;
        }
        else if(day == "twenty first")
        {
            this.day = 21;
        }
        else if(day == "twenty second")
        {
            this.day = 22;
        }
        else if(day == "twenty third")    
        {
            this.day = 23;
        }
        else if(day == "twenty fourth")
        {
            this.day = 24;
        }
        else if(day == "twenty fifth")
        {
            this.day = 25;
        }
        else if(day == "twenty sixth")
        {
            this.day = 26;
        }
        else if(day == "twenty seventh")
        {
            this.day = 27;
        }
        else if(day == "twenty eighth")
        {
            this.day = 28;
        }
        else if(day == "twenty ninth")
        {
            this.day = 29;
        }
        else if(day == "thirtieth")
        {
            this.day = 30;
        }
        else if(day == "thirty first")
        {
            this.day = 31;
        }
    }

    /**
     * @return the month
     */
    public int getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(int month) {
        if(month >= 1 && month<=12)
        {
            this.month = month;
        }
        else
        {
            System.out.println("\nMonth is not valid");
        }
    }
    
    public void setMonth(String month)
    {
        if(month == "January")
        {
            this.month = 1;
        }
        else if(month == "February")
        {
            this.month = 2;
        }
        else if(month == "March")
        {
            this.month = 3;
        }
        else if(month == "April")
        {
            this.month = 4;
        }
        else if(month == "May")
        {
            this.month = 5;
        }
        else if(month == "June")
        {
            this.month = 6;
        }
        else if(month == "July")
        {
            this.month = 7;
        }
        else if(month == "August")
        {
            this.month = 8;
        }
        else if(month == "September")
        {
            this.month = 9;
        }
        else if(month == "October")
        {
            this.month = 10;
        }
        else if(month == "November")
        {
            this.month = 11;
        }
        else if(month == "December")
        {
            this.month = 12;
        }
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }
    
    public  boolean Check_year()
    {
	if (((this.year % 4 == 0) && (this.year % 100!= 0)) || (this.year%400 == 0))
	{
	    return true;
        }
	else
        {
            return false;
        }
    }
   
    public MyDate()
    {
        this.setDay(24);
        this.setMonth(12);
        this.setYear(2020);
    }
    public MyDate(int day,int month,int year)
    {
        this.setDay(day);
        this.setMonth(month);
        this.setYear(year);
    }
    public int get_month(String month)
    {
        int temp=0;
        switch(month)
        {
                case "January" : temp = 1; break;
                case "February" : temp = 2; break;
                case "March" : temp = 3; break;
                case "April" : temp = 4; break;
                case "May"  :temp = 5; break;
                case "June" :temp = 6; break;
                case "July" :temp = 7; break;
                case "August" :temp = 8; break;
                case "September" :temp = 9; break;
                case "October" :temp = 10; break;
                case "November" :temp = 11; break;
                case "December" :temp = 12; break;
        }
        return temp;
    }
    public MyDate(String date)
    {
        String[] str = date.split(" ",3);
        String temp = str[1];
        String day;
        char a = temp.charAt(1);
        if(a >= 'a' && a <= 'z')
        {
            day = temp.substring(0,1);
        }
        else
        {
            day = temp.substring(0, 2);
        }
        this.setDay(Integer.parseInt(day));
        int month = get_month(str[0]);
        this.setMonth(month);
        int year = Integer.parseInt(str[2]);
        this.setYear(year);
    }
    public void accept()
    {   
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter your date:");
        String date = sc.nextLine();
        String[] str = date.split(" ",3);
        String temp = str[1];
        String day;
        char a = temp.charAt(1);
        if(a >= 'a' && a <= 'z')
        {
            day = temp.substring(0,1);
        }
        else
        {
            day = temp.substring(0, 2);
        }
        this.setDay(Integer.parseInt(day));
        int month = get_month(str[0]);
        this.month = month;
        int year = Integer.parseInt(str[2]);
        this.year = year;
    }
    public void print()
    {
        switch(this.month)
        {
                case 1 : System.out.print("January"); break;
                case 2 : System.out.print("February"); break;
                case 3 : System.out.print("March"); break; 
                case 4 : System.out.print("April"); break;
                case 5  : System.out.print("May"); break;
                case 6 : System.out.print("June"); break;
                case 7 : System.out.print("July"); break;
                case 8 : System.out.print("August"); break;
                case 9 : System.out.print("September"); break;
                case 10 : System.out.print("October"); break;
                case 11 : System.out.print("November"); break;
                case 12 : System.out.print("December"); break;
        }
        
        switch(this.day)
        {
                case 1 : System.out.print(" first"); break;
                case 2 : System.out.print(" second"); break;
                case 3 : System.out.print(" third"); break; 
                case 4 : System.out.print(" fourth"); break;
                case 5  : System.out.print(" fifth"); break;
                case 6 : System.out.print(" sixth"); break;
                case 7 : System.out.print(" seventh"); break;
                case 8 : System.out.print(" eighth"); break;
                case 9 : System.out.print(" ninth"); break;
                case 10 : System.out.print(" tenth"); break;
                case 11 : System.out.print(" eleventh"); break;
                case 12 : System.out.print(" twelfth"); break;
                case 13 : System.out.print(" thirteenth"); break;
                case 14 : System.out.print(" foruteenth"); break;
                case 15 : System.out.print(" fifteenth"); break; 
                case 16 : System.out.print(" sixteenth"); break;
                case 17  : System.out.print(" seventeenth"); break;
                case 18 : System.out.print(" eighteenth"); break;
                case 19 : System.out.print(" nineteenth"); break;
                case 20 : System.out.print(" twentieth"); break;
                case 21 : System.out.print(" twenty first"); break;
                case 22 : System.out.print(" twenty second"); break;
                case 23 : System.out.print(" twenty third"); break;
                case 24 : System.out.print(" twenty fourth"); break;
                case 25 : System.out.print(" twenty fifth"); break;
                case 26 : System.out.print(" twenty sixth"); break;
                case 27 : System.out.print(" twenty seventh"); break; 
                case 28 : System.out.print("twenty eighth"); break;
                case 29 : System.out.print(" twenty ninth"); break;
                case 30 : System.out.print(" thirtieth"); break;
                case 31 : System.out.print(" thirty first"); break;
        }
        System.out.print(" " + this.year);
    }
    public void print(int format)
    {
        if(format == 1)
        {
            System.out.printf("%d/%d/%d",this.day,this.month,this.year);
        }
        else if(format == 2)
        {
            String year = Integer.toString(this.year);
            String temp = year.substring(2,4);
            System.out.printf("%s-%d-%d",temp,this.month,this.day);
        }
        else if(format == 3)
        {
            this.print();
        }
    }
}
