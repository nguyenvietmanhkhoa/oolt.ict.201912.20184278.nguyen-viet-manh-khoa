package hust.soict.globalict.aims;

import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author admin
 */
public class Aims {
    public static void main(String args[])
    {
        Order anOrder = new Order();
        
        anOrder.setDateOrdered("25/3/2020");
        
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        
        dvd1.setCategory("Animation");
        
        dvd1.setCost(19.95f);
        
        dvd1.setDirector("Roger Allers");
        
        dvd1.setLength(87);
        
        
        anOrder.addDigitalVideoDisc(dvd1);
        
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        
        dvd2.setCategory("Science Fiction");
        
        dvd2.setCost(24.95f);
        
        dvd2.setDirector("George Lucas");
        
        dvd2.setLength(124);
        
        anOrder.addDigitalVideoDisc(dvd2);
        
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin");
        
        dvd3.setCategory("Animation");
        
        dvd3.setCost(18.99f);
        
        dvd3.setDirector("John Musker");
        
        dvd3.setLength(90);
        
        anOrder.addDigitalVideoDisc(dvd3);
        
        DigitalVideoDisc dvd_free = anOrder.getALuckyItem();
        
        System.out.print(dvd_free.getTitle());
        
        anOrder.print_format(dvd_free);
    }
}
