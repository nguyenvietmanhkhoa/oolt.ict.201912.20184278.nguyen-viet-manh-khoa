package hust.soict.globalict.aims.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author admin
 */
public class DateUtils {
    public static int compare(MyDate date1,MyDate date2)  // return 1 if date1 > date 2,0 if date1 = date2,-1 if date1 < date2
    {
        if(date1.getYear() > date2.getYear())
        {
            return 1;
        }
        else if(date1.getYear() < date2.getYear())
        {
            return -1;
        }
        else
        {
            if(date1.getMonth() > date2.getMonth())
            {
                return 1;
            }
            else if(date1.getMonth() < date2.getMonth())
            {
                return -1;
            }
            else
            {
                if(date1.getDay() > date2.getDay())
                {
                   return 1;   
                }
                else if(date1.getDay() < date2.getDay())
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    public static void Sorting(MyDate[] mydate)
    {
        for(int i=0;i<mydate.length-1;i++)
        {
            for(int j=i+1;j<mydate.length;j++)
            {
                if(compare(mydate[i],mydate[j])==1)
                {
                    MyDate temp = mydate[i];
                    mydate[i] = mydate[j];
                    mydate[j] = temp;
                }
            }
        }
    }
}
