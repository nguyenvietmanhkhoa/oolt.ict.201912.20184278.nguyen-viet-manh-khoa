package hust.soict.globalict.aims.order;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author admin
 */
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import java.util.Arrays;
public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    
    private int qtyOrdered = 0;
     
    private String dateOrdered;
    
    public static final int MAX_LIMITTED_ORDERS = 5;
    
    private static int nbOrders = 0;
    
    public DigitalVideoDisc[] itemsOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    
        /**
     * @return the qtyOrdered
     */
    public int getQtyOrdered() {
        return qtyOrdered;
    }

    /**
     * @param qtyOrdered the qtyOrdered to set
     */
    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    
    public void addDigitalVideoDisc(DigitalVideoDisc disc)
    {
        this.itemsOrdered[this.getQtyOrdered()] = disc;
        int n = this.getQtyOrdered() + 1;
        this.setQtyOrdered(n);
    }
    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList)
    {
        //Check if the number of DVDs of the current order does not reach the maximum
        int no_dvd = dvdList.length;
        if(no_dvd + this.qtyOrdered > MAX_NUMBERS_ORDERED)
        {
            System.out.println("Cannot order more because full of ordered dvds");
            return;
        }
        for(int i=0;i<no_dvd;i++)
        {
            this.itemsOrdered[this.qtyOrdered] = dvdList[i];
            this.qtyOrdered++;
        }
    }
    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2)
    {
        if(this.qtyOrdered <= MAX_NUMBERS_ORDERED)
        {
            this.addDigitalVideoDisc(dvd1);
            this.qtyOrdered++;
            if(this.qtyOrdered <= MAX_NUMBERS_ORDERED)
            {
                this.addDigitalVideoDisc(dvd2);
            }
            else
            {
                System.out.println("Cannot order more because full of ordered dvds");
                System.out.println("DVD " + dvd2.getTitle() + " cannot be added");
            }
        }
        else
        {
            System.out.println("Cannot order more because full of ordered dvds");
            System.out.println("DVD " + dvd1.getTitle() + " DVD " + dvd2.getTitle() + " cannot be added");
        }
    }
    public void removeDigitalVideoDisc(DigitalVideoDisc disc)
    {
        for(int i=0;i<this.getQtyOrdered();i++)
        {
            if(this.itemsOrdered[i] == disc)
            {
                for(int j=i;j<this.getQtyOrdered()-1;j++)
                {
                    this.itemsOrdered[j] = this.itemsOrdered[j+1];
                }
            }
        }
        this.setQtyOrdered(this.getQtyOrdered()-1);
    }
    public float totalCost()
    {
        float sum = 0;
        for(int i=0;i<this.getQtyOrdered();i++)
        {
            sum+=this.itemsOrdered[i].getCost();
        }   
        return sum;
    }
    public float totalCost(DigitalVideoDisc lucky_item)
    {
        float sum = 0;
        for(int i=0;i<this.getQtyOrdered();i++)
        {
            if(this.itemsOrdered[i]!=lucky_item)
            {
                sum+=this.itemsOrdered[i].getCost();
            }
        }   
        return sum;
    }
    public void print_format()
    {
        if(nbOrders <= MAX_NUMBERS_ORDERED)
        {
        System.out.println("*****************Order*****************");
        System.out.println("Date:" + this.dateOrdered);
        System.out.println("Ordered Items:");
        for(int i=0;i<this.qtyOrdered;i++)
        {
            System.out.printf("%d.%s-%s-%s-%d:%.2f $\n",i+1,this.itemsOrdered[i].getTitle(),this.itemsOrdered[i].getDirector(),this.itemsOrdered[i].getCategory(),this.itemsOrdered[i].getLength(),this.itemsOrdered[i].getCost());
        }
        System.out.println("Total cost:" + this.totalCost() + " $");
        System.out.println("***************************************");
        }
        else
        {
            System.out.println("You cannot order any more because of reaching the maximum order!!!");
        }
        nbOrders++;
    }
    
    public void print_format(DigitalVideoDisc lucky_item)
    {
        if(nbOrders <= MAX_NUMBERS_ORDERED)
        {
        System.out.println("*****************Order*****************");
        System.out.println("Date:" + this.dateOrdered);
        System.out.println("Ordered Items:");
        for(int i=0;i<this.qtyOrdered;i++)
        {
            if (this.itemsOrdered[i] == lucky_item)
            {
                System.out.printf("%d.LUCKY ITEM:%s-%s-%s-%d:%.2f $\n",i+1,this.itemsOrdered[i].getTitle(),this.itemsOrdered[i].getDirector(),this.itemsOrdered[i].getCategory(),this.itemsOrdered[i].getLength(),this.itemsOrdered[i].getCost());
            }
            else
            {
                System.out.printf("%d.%s-%s-%s-%d:%.2f $\n",i+1,this.itemsOrdered[i].getTitle(),this.itemsOrdered[i].getDirector(),this.itemsOrdered[i].getCategory(),this.itemsOrdered[i].getLength(),this.itemsOrdered[i].getCost());
            }
        }
        System.out.println("Total cost:" + this.totalCost(lucky_item) + " $");
        System.out.println("***************************************");
        }
        else
        {
            System.out.println("You cannot order any more because of reaching the maximum order!!!");
        }
        nbOrders++;
    }


    /**
     * @return the dateOrdered
     */
    public String getDateOrdered() {
        return dateOrdered;
    }

    /**
     * @param dateOrdered the dateOrdered to set
     */
    public void setDateOrdered(String dateOrdered) {
        this.dateOrdered = dateOrdered;
    }
    public DigitalVideoDisc getALuckyItem()
    {
        int lucky_number  = -1;
        while(lucky_number < 0 || lucky_number > this.qtyOrdered-1)
        {
            
            lucky_number = (int)(10.0 * Math.random());
        }
        return this.itemsOrdered[lucky_number];
    }
}
