package hust.soict.globalict.lab02;


public class Lab02_ex6 {
	public static void Sort(int[] array)
	{
		for(int i=0;i<array.length-1;i++)
		{
			for(int j=i+1;j<array.length;j++)
			{
				if(array[i]>array[j])
				{
					int temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
	}
	public static void operation_in_array(int[] array)
	{
		int sum=0;
		double average;
		for(int elements : array)
		{
			sum += elements;
		}
		int n = array.length;
		average =(double) sum/n;
		System.out.println("The sum and average value of the array are " + sum + " and " + average);
		Sort(array);
		for(int elements : array)
		{
			System.out.print(elements + " ");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] array = new int[]{5,3,2,4,1};
		operation_in_array(array);
	}

}
