package hust.soict.globalict.lab02;


public class Lab02_ex7 {
	public  static int[][] Add_matrices(int[][] matrix_1,int[][] matrix_2)
	{
		int[][] matrix = new int[matrix_1.length][matrix_1[0].length];
		for(int i=0;i<matrix_1.length;i++)
		{
			for(int j=0;j<matrix_1[0].length;j++)
			{
				matrix[i][j] = matrix_1[i][j] + matrix_2[i][j];
			}
		}
		return matrix;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] matrix_1 = new int[][] {{1,2,3},{4,5,6},{7,8,9}};
		int[][] matrix_2 = new int[][] {{1,2,3},{4,5,6},{7,8,9}};
		int[][] matrix = new int[3][3];
		matrix = Add_matrices(matrix_1,matrix_2);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(matrix[i][j] + " ");
			}
			System.out.print("\n");
		}
	}

}