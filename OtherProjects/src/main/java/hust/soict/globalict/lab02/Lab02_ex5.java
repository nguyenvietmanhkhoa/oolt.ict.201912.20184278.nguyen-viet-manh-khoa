package hust.soict.globalict.lab02;

import java.util.Scanner;

public class Lab02_ex5 {
	public static boolean Check_year(int year)
	{
		 if (((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0))
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	}
	public static boolean Check_month(int month)
	{
		if(month>=1 && month<=12)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public static void Display_number_of_days(int month,int year)
	{
			if(Check_month(month))
			{
				if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
				{
					System.out.println("Month " + month + " Year " + year + " has 31 days");
				}
				else if(month == 4 || month == 6 || month == 9 || month == 11)
				{
					System.out.println("Month " + month + " Year " + year + " has 30 days");
				}
				else
				{
					if(Check_year(year))
					{
					   System.out.println("Month 2" +" Year " + year + " has 29 days");
					}
					else
					{
					   System.out.println("Month 2" +" Year " + year + " has 28 days");
					}
				}
			}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int month,year;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your month:");
		month = sc.nextInt();
		System.out.println("Enter your year:");
		year = sc.nextInt();
		Display_number_of_days(month,year);
	}

}
