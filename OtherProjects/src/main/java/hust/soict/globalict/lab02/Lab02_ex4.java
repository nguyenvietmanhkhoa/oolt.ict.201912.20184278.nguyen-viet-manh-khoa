package hust.soict.globalict.lab02;

public class Lab02_ex4 {
	public static void Draw_triangle(int n)
	{
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=2*n-1;j++)
			{
				if((j>=n-i+1) && (j<=n+i-1))
				{
					System.out.print("*");
				}
				else
				{
					System.out.print(" ");
				}
			}
			System.out.print("\n");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n=5;
		Draw_triangle(n);
	}

}

