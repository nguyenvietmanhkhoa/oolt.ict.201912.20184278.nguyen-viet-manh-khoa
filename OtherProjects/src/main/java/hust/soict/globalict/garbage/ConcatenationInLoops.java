/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.globalict.garbage;

import com.sun.management.GarbageCollectionNotificationInfo;
import java.io.FileNotFoundException;
import java.util.Random;

/**
 *
 * @author admin
 */
public class ConcatenationInLoops {
    public static void main(String argsp[]) throws FileNotFoundException
    {
        /*
        Random r = new Random(123);
        long start = System.currentTimeMillis();
        String s= "";
        for (int i=0 ;i < 65536;i++)
        {
            s+=r.nextInt();
        }
        System.out.println(System.currentTimeMillis()- start);
        
        r = new Random(123);
        start = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<65536;i++)
        {
            sb.append(r.nextInt(2));
        }
        s = sb.toString();
        System.out.println(System.currentTimeMillis()- start);
        */
        GarbageCreator gc1 = new GarbageCreator();
        
        NoGarbage no_gc1 = new NoGarbage();
        
        gc1.Read_file();
        
        no_gc1.Read_file_buffer();
    }
}
