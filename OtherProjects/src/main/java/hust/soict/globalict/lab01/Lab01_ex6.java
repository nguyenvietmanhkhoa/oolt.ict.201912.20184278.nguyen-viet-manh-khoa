package hust.soict.globalict.lab01;

import static java.lang.Math.sqrt;
public class Lab01_ex6 {

	public static void Solving_first_degree_equation(double a,double b)
	{
		if(a==0)
		{
			if(b==0)
			{
				System.out.println("Equation has infinite solutions!!!");
			}
			else
			{
				System.out.println("Equation has no solution");
			}
		}
		else
		{
			System.out.printf("System has solution:%.2f",-b/a);
		}
	}
	public static void Solving_first_degree_equation(double a,double b,double c)
	{
		if(a==0)
		{
			if(b==0)
			{
				if(c==0)
				{
					System.out.println("Equation has infinite solutions!!!");
				}
				else
				{
					System.out.println("Equation no solution!!!");
				}
			}
			else
			{
				System.out.printf("Equation has solutions with all x and y = %.2f",c/b);
				
			}
		}
		else
		{
			if(b == 0)
			{
				System.out.printf("Equation has solutions with all y and x = %.2f",c/a);
			}
			else
			{
				System.out.printf("Equation has solutions with all x and y = (-a*x+c)/b");
			}
		}
		
	}
	public static void Solving_second_degree_equation(double a,double b,double c)
	{
		if(a==0)
		{
			if(b==0)
			{
				if(c==0)
				{
					System.out.println("Equation has infinite solution");
				}
				else 
				{
					System.out.println("Equation has no solution");
				}
			}
			else
			{
				System.out.printf("Equation has one solution:%.2f",-c/b);
			}
		}
		else
		{
			double delta = b*b - 4*a*c;
			if(delta<0)
			{
				System.out.printf("Equation has no solution!!!");
			}
			else if(delta == 0)
			{
				System.out.printf("Equation has unique solution:%.2f",-b/(2*a));
			}
			else
			{
				double root_1 = (-b-sqrt(delta))/(2*a);
				double root_2 = (-b+sqrt(delta))/(2*a);
				System.out.printf("Equation has 2 solutions:%.2f and %.2f",root_1,root_2);
			}
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double  a = 19.8;
		double b = 20.8;
		double c = 13.4;
		Solving_second_degree_equation(a,b,c);

	}

}
